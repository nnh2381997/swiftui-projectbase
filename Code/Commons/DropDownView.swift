//
//  DropDownView.swift
//  SwiftUI-ProjectBase
//
//  Created by HienNN on 17/08/2023.
//

import SwiftUI

struct DropDownView<O: Hashable>: View {
    let data: [O]
    let title: String
    let value: O?
    let noDataText: String
    let font: Font
    let textColor: Color
    let setValue: (O) -> Void
    let zIndex: Int64
    
    @Binding var showDropDown: Bool
    private let defaultHeight: CGFloat = 64
    
    init(
        title: String,
        value: O? = nil,
        data: [O],
        noDataText: String,
        font: Font = .system(size: 16),
        textColor: Color = .black,
        showDropDown: Binding<Bool>,
        zIndex: Int64 = 10,
        setValue: @escaping (O) -> Void
    ) {
        self.data = data
        self.title = title
        self.value = value
        self.noDataText = noDataText
        self.font = font
        self.textColor = textColor
        self._showDropDown = showDropDown
        self.zIndex = zIndex
        self.setValue = setValue
    }
    
    var body: some View {
        Button(action: {
            withAnimation {
                showDropDown.toggle()
            }
        }) {
            FormGroupView {
                HStack {
                    VStack(alignment: .leading, spacing: 5) {
                        RequiredFieldView {
                            Text(title)
                                .font(font)
                                .textCase(.uppercase)
                                .foregroundColor(textColor)
                        }
                        if let value = value {
                            Text(String(describing: value))
                                .font(.subheadline)
                                .foregroundColor(textColor)
                                .padding(.leading, 15)
                        }
                    }
                    Spacer()
                    Image(systemName: "chevron.down")
                        .font(.headline)
                        .foregroundColor(textColor)
                        .opacity(data.count > 1 ? 1 : 0.4)
                }
                .padding(.horizontal, 5)
            }
        }
        .zIndex(Double(zIndex))
        .frame(maxWidth: .infinity, minHeight: defaultHeight, alignment: .leading)
        .buttonStyle(.plain)
        .overlay(
            VStack {
                if showDropDown {
                    Spacer(minLength: defaultHeight + 4)
                    ListRow(
                        data: data,
                        noDataText: noDataText,
                        action: { selectedValue in
                            withAnimation {
                                if selectedValue != value {
                                    setValue(selectedValue)
                                }
                                showDropDown.toggle()
                            }
                        }
                    )
                    .padding(.horizontal, 5)
                }
            }
            .zIndex(99), alignment: .topLeading
        )
    }
}

struct DropDownView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            DropDownView(
                title: "Number",
                value: 1,
                data: [1, 2, 3, 4, 5, 6, 7],
                noDataText: "No Data",
                showDropDown: .constant(true),
                setValue: { value in
                    
                }
            )
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding()
        .background(.black.opacity(0.3))
    }
}
