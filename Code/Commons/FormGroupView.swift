//
//  FormGroupView.swift
//  SwiftUI-ProjectBase
//
//  Created by HienNN on 17/08/2023.
//

import SwiftUI

struct FormGroupView<Content: View>: View {
    private let cornerRadius: CGFloat
    private let color: Color
    private let borderColor: Color?
    private let borderWidth: CGFloat
    private let disabledBackgroundColor: Color
    private let disabledBrightness: CGFloat

    @ViewBuilder private let content: () -> Content
    @Environment(\.isEnabled) private var isEnabled

    init(
        cornerRadius: CGFloat = 31,
        color: Color = .white,
        borderColor: Color? = nil,
        borderWidth: CGFloat = 2,
        disabledBackgroundColor: Color = .gray.opacity(0.5),
        disabledBrightness: CGFloat = 0.46,
        @ViewBuilder content: @escaping () -> Content
    ) {
        self.cornerRadius = cornerRadius
        self.color = color
        self.borderColor = borderColor
        self.borderWidth = borderWidth
        self.disabledBackgroundColor = disabledBackgroundColor
        self.disabledBrightness = disabledBrightness
        self.content = content
    }

    public var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: cornerRadius)
                .foregroundColor(isEnabled ? color : disabledBackgroundColor)
            content()
                .padding(.horizontal, cornerRadius / 2)
                .clipped()
                .brightness(isEnabled ? 0 : disabledBrightness)
            if let borderColor = borderColor {
                RoundedRectangle(cornerRadius: cornerRadius)
                    .stroke(borderColor, lineWidth: borderWidth)
            }
        }
        .clipShape(RoundedRectangle(cornerRadius: cornerRadius))
        .frame(minHeight: 64)
        .fixedSize(horizontal: false, vertical: true)
    }
}

struct FormGroupView_Previews: PreviewProvider {
    @State static var text: String = ""

    static var previews: some View {
        FormGroupView() {
            HStack {
                RequiredFieldView {
                    VStack(alignment: .leading, spacing: 4) {
                        Text("Name")
                        TextField("", text: $text)
                    }
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding()
        .background(.black.opacity(0.3))
        .previewLayout(.sizeThatFits)
    }
}
