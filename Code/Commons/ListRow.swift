//
//  ListRow.swift
//  SwiftUI-ProjectBase
//
//  Created by HienNN on 17/08/2023.
//

import SwiftUI

struct ListRow<O: Hashable>: View {
    let data: [O]
    let noDataText: String
    let font: Font
    let textColor: Color
    let action: (_ selectedItem: O) -> ()
    
    init(
        data: [O],
        noDataText: String,
        font: Font = .system(size: 15),
        textColor: Color = .black,
        action: @escaping (O) -> ()
    ) {
        self.data = data
        self.noDataText = noDataText
        self.font = font
        self.textColor = textColor
        self.action = action
    }
    
    private var getHeight: CGFloat {
        if self.data.count < 2 {
            return 50
        } else  if self.data.count < 3 {
            return 100
        }
        return 150
    }
    
    var body: some View {
        ScrollView {
            LazyVStack(alignment: .leading, spacing: 0) {
                if data.isEmpty {
                    HStack {
                        Text(noDataText)
                            .font(font)
                            .foregroundColor(textColor)
                            .tracking(0.6)
                            .padding(.vertical)
                    }
                } else {
                    ForEach(data, id: \.self) { item in
                        HStack(alignment: .center) {
                            Text(String(describing: item))
                                .font(font)
                                .foregroundColor(textColor)
                                .tracking(0.6)
                                .padding(.vertical)
                        }
                        .frame(maxWidth: .infinity, maxHeight: 50, alignment: .leading)
                        .padding(.horizontal)
                        .border(width: 1, edges: [.bottom], color: .black.opacity(0.1))
                        .contentShape(Rectangle())
                        .onTapGesture {
                            action(item)
                        }
                    }
                }
            }
        }
        .frame(height: getHeight)
        .background(SwiftUI.Color.white.shadow())
    }
}

struct ListRow_Previews: PreviewProvider {
    static var previews: some View {
        ListRow(
            data: [1, 2, 3],
            noDataText: "No Data",
            action: { _ in }
        )
        .tint(.black)
        .previewLayout(.fixed(width: 600, height: 200))
    }
}
