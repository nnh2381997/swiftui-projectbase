//
//  LoadingView.swift
//  SwiftUI-ProjectBase
//
//  Created by HienNN on 31/08/2023.
//

import SwiftUI

struct LoadingView: View {
    let image: ImageAsset
    let size: CGFloat
    let roundSize: CGFloat
    let tranparentColor: Color
    let circleColor: Color
    @State var isStart: Bool = false
    
    init(
        image: ImageAsset,
        size: CGFloat = 60,
        tranparentColor: Color = .black.opacity(0.5),
        circleColor: Color = .red
    ) {
        self.image = image
        self.size = size
        self.roundSize = size + 5
        self.tranparentColor = tranparentColor
        self.circleColor = circleColor
    }
    
    var body: some View {
        ZStack {
            Image(image)
                .resizable()
                .frame(width: size, height: size)
                .background(Capsule().fill(.white))
            
            Circle()
                .trim(from: 0, to: 0.5)
                .stroke(circleColor, lineWidth: 3)
                .frame(width: roundSize, height: roundSize)
                .rotationEffect(Angle(degrees: isStart ? 360 : 0))
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(tranparentColor)
        .onAppear {
            withAnimation(Animation.linear(duration: 1.5).repeatForever(autoreverses: false)) {
                isStart.toggle()
            }
        }
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView(
            image: Asset.logoSave
        )
    }
}
