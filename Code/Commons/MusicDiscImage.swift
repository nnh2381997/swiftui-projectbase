//
//  MusicDiscImage.swift
//  SwiftUI-ProjectBase
//
//  Created by MacBook on 04/09/2023.
//

import SwiftUI

struct MusicDiscImage: View {
    let width: CGFloat
    let height: CGFloat
    @Binding var isStartAnimation: Bool
    
    init(
        width: CGFloat = 300,
        height: CGFloat = 300,
        isStartAnimation: Binding<Bool>
    ) {
        self.width = width
        self.height = height
        self._isStartAnimation = isStartAnimation
    }
    
    
    var body: some View {
        ZStack {
            Image(Asset.image01)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .clipShape(Circle())
                .rotationEffect(Angle(degrees: isStartAnimation ? 360 : 0))
                .animation(isStartAnimation ? Animation.linear(duration: 5).repeatForever(autoreverses: false) : Animation.linear, value: isStartAnimation)
        }
        .frame(width: width, height: height)
        .background(Capsule().fill(.red))
    }
}

struct MusicDiscImage_Previews: PreviewProvider {
    @State static private var isStart: Bool = false
    
    static var previews: some View {
        MusicDiscImage(isStartAnimation: $isStart)
    }
}
