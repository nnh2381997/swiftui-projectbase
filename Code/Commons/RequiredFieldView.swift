//
//  RequiredFieldView.swift
//  SwiftUI-ProjectBase
//
//  Created by HienNN on 17/08/2023.
//

import SwiftUI

public struct RequiredFieldView<Content: View>: View {
    @ViewBuilder let content: () -> Content

    public var body: some View {
        HStack(alignment: .top, spacing: 0) {
            Image(systemName: "asterisk")
                .font(.system(size: 5, weight: .black))
                .foregroundColor(.red)
                .padding(.trailing, 3)
            content()
        }
    }
}

struct RequiredFieldView_Previews: PreviewProvider {
    static var previews: some View {
        RequiredFieldView() {
            Text("Must have")
        }
    }
}
