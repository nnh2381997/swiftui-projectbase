//
//  TextFieldView.swift
//  SwiftUI-ProjectBase
//
//  Created by HienNN on 18/08/2023.
//

import SwiftUI

struct TextFieldView: View {
    let title: String
    let fontTitle: Font
    let fontValue: Font
    let rightIcon: String?
    
    @Binding var valueField: String
    
    init(
        title: String,
        valueField: Binding<String>,
        rightIcon: String? = nil,
        fontTitle: Font = .headline,
        fontValue: Font = .system(size: 15)
    ) {
        self.title = title
        self._valueField = valueField
        self.rightIcon = rightIcon
        self.fontTitle = fontTitle
        self.fontValue = fontValue
    }
    
    var body: some View {
        FormGroupView {
            HStack(alignment: .center) {
                VStack(alignment: .leading, spacing: 3) {
                    RequiredFieldView {
                        Text(title)
                            .font(fontTitle)
                    }
                    TextField("", text: $valueField)
                        .font(fontValue)
                        .textFieldStyle(.plain)
                        .autocorrectionDisabled()
                        .padding(.vertical, 3)
                        .padding(.leading, 10)
                }
                .padding(.horizontal, 5)
                if let rightIcon = rightIcon {
                    Image(systemName: rightIcon)
                        .font(fontTitle)
                }
            }
        }
    }
}

struct TextFieldView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            TextFieldView(title: "Name", valueField: .constant("hien"))
        }
        .padding()
        .background(.black.opacity(0.5))
    }
}
