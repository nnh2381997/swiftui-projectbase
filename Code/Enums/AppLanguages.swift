//
//  AppLanguages.swift
//  SwiftUI-ProjectBase
//
//  Created by HienNN on 17/08/2023.
//

import Foundation

public enum AppLanguages: String, CaseIterable {
    case en
    case ja
    case vi
    
    public init?(intValue: Int?) {
        guard let value = intValue else { return nil }
        switch value {
        case 0:
            self = .en
        case 1:
            self = .ja
        case 2:
            self = .vi
        default:
            return nil
        }
    }
}

extension AppLanguages: CustomStringConvertible {
    public static let APPLANGUAGE = "AppLanguage"
    
    public var description: String {
        switch self {
        case .en:
            return "English"
        case .ja:
            return "日本語"
        case .vi:
            return "Vietnamese"
        }
    }
    
    public var intValue: Int {
        switch self {
        case .en:
            return 0
        case .ja:
            return 1
        case .vi:
            return 2
        }
    }
    
    public static var getAppLanguage: AppLanguages {
        guard let languageString = UserDefaults.standard.string(forKey: Self.APPLANGUAGE),
              let language = AppLanguages(rawValue: languageString)
        else {
            #if os(macOS)
                return AppLanguages(rawValue: Locale.current.language.languageCode?.identifier ?? "") ?? .en
            #else
                return AppLanguages(rawValue: Locale.current.languageCode ?? "") ?? .en
            #endif
        }
        return language
    }
    
    public static func saveAppLanguage(_ appLanguages: AppLanguages?) {
        let language = appLanguages ?? .en
        UserDefaults.standard.setValue(language.rawValue, forKey: Self.APPLANGUAGE)
    }
}
