//
//  DateExtensions.swift
//  SwiftUI-ProjectBase
//
//  Created by HienNN on 17/08/2023.
//

import Foundation

public extension Date {
    var convertDateToString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = self as Date
        return dateFormatter.string(from: date)
    }
    
    var formatterDate: Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = self as Date
        return date
    }
}
