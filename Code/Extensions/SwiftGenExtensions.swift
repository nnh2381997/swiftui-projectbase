//
//  SwiftGenExtensions.swift
//  Views
//
//  Created by Dwight Everhart on 1/16/22.
//  Copyright © 2022 TaylorMade Golf. All rights reserved.
//

import SwiftUI

extension ColorAsset {
    var swiftUIColor: SwiftUI.Color {
        SwiftUI.Color(uiColor: color)
    }
}

extension Color {
    init(_ colorAsset: ColorAsset) {
        self.init(uiColor: colorAsset.color)
    }
    
    init(hex: String) {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int: UInt64 = 0
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }

        self.init(
            .sRGB,
            red: Double(r) / 255,
            green: Double(g) / 255,
            blue:  Double(b) / 255,
            opacity: Double(a) / 255
        )
    }
}

//extension Font {
//    static func regular(_ size: CGFloat) -> Font {
//        Font.custom(FontFamily.DINNextLTPro.regular.name, size: size)
//    }
//
//    static func bold(_ size: CGFloat) -> Font {
//        Font.custom(FontFamily.DINNextLTPro.bold.name, size: size)
//    }
//    
//    static func clubHausRegular(_ size: CGFloat) -> Font {
//        Font.custom(FontFamily.ClubHaus.regular.name, size: size)
//    }
//    
//    static func heavyCondensed(_ size: CGFloat) -> Font {
//        Font.custom(FontFamily.DINNextLTPro.heavyCondensed.name, size: size)
//    }
//}

extension Image {
    init(_ imageAsset: ImageAsset) {
        self.init(imageAsset.name)
    }
}

extension Shape {
    func stroke(_ color: ColorAsset, lineWidth: CGFloat = 1) -> some View {
        stroke(color.swiftUIColor, lineWidth: lineWidth)
    }

    func stroke(_ color: ColorAsset, style: StrokeStyle) -> some View {
        stroke(color.swiftUIColor, style: style)
    }

    func fill(_ color: ColorAsset, style: FillStyle = FillStyle()) -> some View {
        fill(color.swiftUIColor, style: style)
    }
}

extension View {
    func background(_ color: ColorAsset) -> some View {
        background(color.swiftUIColor)
    }

    func foregroundColor(_ color: ColorAsset) -> some View {
        foregroundColor(color.swiftUIColor)
    }
    
    func tint(_ color: ColorAsset) -> some View {
        tint(color.swiftUIColor)
    }
}
