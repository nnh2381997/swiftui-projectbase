//
//  LiveNetworkingService.swift
//  SwiftUI-ProjectBase
//
//  Created by HienNN on 17/08/2023.
//

import Foundation
import Combine

public typealias ObjectModel = Codable & Equatable & Identifiable

public enum HTTPMethod: String {
    case get = "GET"
    case put = "PUT"
    case post = "POST"
}

public final class LiveNetworkingService: NetworkingService {
    public private(set) var baseURL: URL = URL(string: "https://google.com")!
    public private(set) var authToken: String?
    
    
    public func setBaseURL(url: String) {
        guard let url = URL(string: url) else {
            print("BaseURL: Failed")
            return
        }
        baseURL = url
    }
    
    public func setAuthToken(token: String?) {
        guard let token = token else {
            print("Token: Failed")
            return
        }
        authToken = token
    }

    public func getApi<O: ObjectModel>(_ path: String, headers: [String : String]?, queryParameters: [String : String]?, isAuthorization: Bool) async throws -> O {
        let data = try await get(path: path, headers: headers, queryParameters: queryParameters, isAuthorization: isAuthorization)
        return try Self.decode(data)
    }

    public func getApi<O: ObjectModel>(_ path: String, headers: [String : String]?, queryParameters: [String : String]?, isAuthorization: Bool) async throws -> [O] {
        let data = try await get(path: path, headers: headers, queryParameters: queryParameters, isAuthorization: isAuthorization)
        return try Self.decode(data)
    }

    public func postApi<O: ObjectModel>(_ path: String, headers: [String: String]?, queryParameters: [String: String]?, body: Data?, isAuthorization: Bool) async throws -> O {
        let data = try await post(path: path, headers: headers, queryParameters: queryParameters, body: body, isAuthorization: isAuthorization)
        return try Self.decode(data)
    }

    public func postApi<O: ObjectModel>(_ path: String, headers: [String: String]?, queryParameters: [String: String]?, body: Data?, isAuthorization: Bool) async throws -> [O] {
        let data = try await post(path: path, headers: headers, queryParameters: queryParameters, body: body, isAuthorization: isAuthorization)
        return try Self.decode(data)
    }
    
    public func putApi<O: ObjectModel>(_ path: String, headers: [String: String]?, queryParameters: [String: String]?, body: Data?, isAuthorization: Bool) async throws -> O {
        let data = try await put(path: path, headers: headers, queryParameters: queryParameters, body: body, isAuthorization: isAuthorization)
        return try Self.decode(data)
    }
    
    public func putApi<O: ObjectModel>(_ path: String, headers: [String: String]?, queryParameters: [String: String]?, body: Data?, isAuthorization: Bool) async throws -> [O] {
        let data = try await put(path: path, headers: headers, queryParameters: queryParameters, body: body, isAuthorization: isAuthorization)
        return try Self.decode(data)
    }
}

private extension LiveNetworkingService {
    func get(path: String, headers: [String: String]? = nil, queryParameters: [String: String]? = nil, isAuthorization: Bool) async throws -> Data {
        return try await requestSync(.get, path: path, headers: headers, queryParameters: queryParameters, isAuthorization: isAuthorization)
    }

    func put(path: String, headers: [String: String]? = nil, queryParameters: [String: String]? = nil, body: Data? = nil, isAuthorization: Bool) async throws -> Data {
        return try await requestSync(.put, path: path, headers: headers, queryParameters: queryParameters,body: body, isAuthorization: isAuthorization)
    }

    func post(path: String, headers: [String: String]? = nil, queryParameters: [String: String]? = nil, body: Data? = nil, isAuthorization: Bool) async throws -> Data {
        return try await requestSync(.post, path: path, headers: headers, queryParameters: queryParameters, body: body, isAuthorization: isAuthorization)
    }
    
    private func requestSync(_ method: HTTPMethod, path: String, headers: [String: String]? = nil, queryParameters: [String: String]? = nil, body: Data? = nil, isAuthorization: Bool) async throws -> Data {
        var result: Result<Data, Error>?
        result = request(method, path: path, headers: headers, queryParameters: queryParameters, body: body, isAuthorization: isAuthorization)
        switch result! {
        case .success(let data):
            return data
        case .failure(let error):
            throw error
        }
    }
    
    func request(_ method: HTTPMethod, path: String, headers: [String: String]? = nil, queryParameters: [String: String]? = nil, body: Data? = nil, isAuthorization: Bool) -> Result<Data, Error> {
        var result: Result<Data, Error>!
        let semaphore = DispatchSemaphore(value: 0)

        var components = URLComponents(url: baseURL.appendingPathComponent(path), resolvingAgainstBaseURL: false)!
        if let queryParameters = queryParameters {
            components.queryItems = queryParameters.map { key, value in
                URLQueryItem(name: key, value: value)
            }
        }

        var request = URLRequest(url: components.url!)
        request.httpMethod = method.rawValue
        
        if let headers = headers {
            headers.forEach { key, value in
                request.addValue(value, forHTTPHeaderField: key)
            }
        }
        
        if isAuthorization {
            guard let authToken = self.authToken else {
                result = .failure(NetworkingError(type: .unauthorized, message: "Authorization failed."))
                semaphore.signal()
                return result
            }
            request.addValue("Bearer \(String(describing: authToken))", forHTTPHeaderField: "Authorization")
        }
        
        if let body = body {
            request.httpBody = body
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        let callStart = Date()
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            let callEnd = Date()
            let dataCount = data?.count ?? 0
            let httpCode = (response as? HTTPURLResponse)?.statusCode ?? 999
            let durationString = callEnd.timeIntervalSince(callStart).formatted(.number.precision(.fractionLength(3)))
            
            print("↓ [\(method.rawValue)] \(request) returned \(dataCount) bytes with status \(httpCode) after \(durationString) seconds")
            
            if let error = error {
                result = .failure(error)
                semaphore.signal()
                return
            }

            guard let data = data else {
                result = .failure(NetworkingError(type: .badStatus(httpCode), message: "No data returned"))
                semaphore.signal()
                return
            }

            guard httpCode == 200 else {
                result = .failure(NetworkingError(type: .badStatus(httpCode), body: data))
                semaphore.signal()
                return
            }
            result = .success(data)
            semaphore.signal()
        }
        task.resume()
        semaphore.wait()
        return result
    }
}

public extension LiveNetworkingService {

    static func encode<DTO: Encodable>(_ dto: DTO) throws -> Data {
        do {
            return try JSONEncoder().encode(dto)
        } catch {
            throw error
        }
    }

    static func decode<DTO: Decodable>(_ data: Data) throws -> DTO {
        do {
            return try JSONDecoder().decode(DTO.self, from: data)
        } catch {
            throw error
        }
    }
}

