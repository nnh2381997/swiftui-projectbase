//
//  NetworkingService.swift
//  SwiftUI-ProjectBase
//
//  Created by HienNN on 17/08/2023.
//

import Foundation
import Combine

public protocol NetworkingService: AnyObject {
    func setBaseURL(url: String)
    func setAuthToken(token: String?)
    
    func getApi<O: ObjectModel>(_ path: String, headers: [String: String]?, queryParameters: [String: String]?, isAuthorization: Bool) async throws -> O
    func getApi<O: ObjectModel>(_ path: String, headers: [String: String]?, queryParameters: [String: String]?, isAuthorization: Bool) async throws -> [O]
    
    func postApi<O: ObjectModel>(_ path: String, headers: [String: String]?, queryParameters: [String: String]?, body: Data?, isAuthorization: Bool) async throws -> O
    func postApi<O: ObjectModel>(_ path: String, headers: [String: String]?, queryParameters: [String: String]?, body: Data?, isAuthorization: Bool) async throws -> [O]
    
    func putApi<O: ObjectModel>(_ path: String, headers: [String: String]?, queryParameters: [String: String]?, body: Data?, isAuthorization: Bool) async throws -> O
    func putApi<O: ObjectModel>(_ path: String, headers: [String: String]?, queryParameters: [String: String]?, body: Data?, isAuthorization: Bool) async throws -> [O]
}

extension NetworkingService {
    public func getApi<O: ObjectModel>(_ path: String, headers: [String: String]? = nil, queryParameters: [String: String]? = nil, isAuthorization: Bool = true) async throws -> O {
        return try await self.getApi(path, headers: headers, queryParameters: queryParameters, isAuthorization: isAuthorization)
    }
    
    public func getApi<O: ObjectModel>(_ path: String, headers: [String: String]? = nil, queryParameters: [String: String]? = nil, isAuthorization: Bool = true) async throws -> [O] {
        return try await self.getApi(path, headers: headers, queryParameters: headers, isAuthorization: isAuthorization)
    }
    
    public func postApi<O: ObjectModel>(_ path: String, headers: [String: String]? = nil, queryParameters: [String: String]? = nil, body: Data? = nil, isAuthorization: Bool = true) async throws -> O {
        return try await self.postApi(path, headers: headers, queryParameters: queryParameters, body: body, isAuthorization: isAuthorization)
    }
    
    public func postApi<O: ObjectModel>(_ path: String, headers: [String: String]? = nil, queryParameters: [String: String]? = nil, body: Data? = nil, isAuthorization: Bool = true) async throws -> [O] {
        return try await self.postApi(path, headers: headers, queryParameters: queryParameters, body: body, isAuthorization: isAuthorization)
    }
    
    public func putApi<O: ObjectModel>(_ path: String, headers: [String: String]? = nil, queryParameters: [String: String]? = nil, body: Data? = nil, isAuthorization: Bool = true) async throws -> O {
        return try await self.postApi(path, headers: headers, queryParameters: queryParameters, body: body, isAuthorization: isAuthorization)
    }
    
    public func putApi<O: ObjectModel>(_ path: String, headers: [String: String]? = nil, queryParameters: [String: String]? = nil, body: Data? = nil, isAuthorization: Bool = true) async throws -> [O] {
        return try await self.postApi(path, headers: headers, queryParameters: queryParameters, body: body, isAuthorization: isAuthorization)
    }
}
