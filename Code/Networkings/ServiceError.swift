//
//  ServiceError.swift
//  SwiftUI-ProjectBase
//
//  Created by HienNN on 17/08/2023.
//

import Foundation

public enum ServiceError: Error, LocalizedError {
    case networkError
    case serverError(_ any: Any)
    case missingFields(_ dtoType: Any, _ id: String? = nil)
}

public extension ServiceError {
    var errorDescription: String? {
        switch self {
        case .networkError:
            return ""
        case let .serverError(any):
            return String(describing: any)
        case let .missingFields(dtoType, id):
            let typeName = String(describing: dtoType)
            return [typeName, id, "lacks required fields"]
                .compactMap { $0 }
                .joined(separator: " ")
        }
    }
}

public struct NetworkingError: Error, Equatable {
    public enum NetworkingErrorType: Equatable {
        case badURL
        case badStatus(_ statusCode: Int)
        case headerNotFound
        case parseFailure
        case unauthorized
        case offline
    }

    public let type: NetworkingErrorType
    public let timestamp: Date
    public let message: String?

    public init(
        type: NetworkingError.NetworkingErrorType,
        timestamp: Date = Date().formatterDate,
        message: String? = nil
    ) {
        self.type = type
        self.timestamp = timestamp
        self.message = message
    }
}

public extension NetworkingError {
    struct ErrorResponse: Decodable {
        let timestamp: Date?
        let message: String?
    }

    init(type: NetworkingErrorType, body: Data) {
        self.type = type

        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .iso8601Custom

        do {
            let errorResponse = try jsonDecoder.decode(ErrorResponse.self, from: body)
            self.timestamp = errorResponse.timestamp ?? Date().formatterDate
            self.message = errorResponse.message
        } catch {
            print("💔 Failed to decode error response: \(error)")
            self.timestamp = Date().formatterDate
            self.message = nil
        }
    }
}

extension JSONDecoder.DateDecodingStrategy {
    public static var iso8601Custom: JSONDecoder.DateDecodingStrategy {
        return .custom { decoder -> Date in
            let container = try decoder.singleValueContainer()
            let dateStr = try container.decode(String.self)
            let dateFormatter = ISO8601DateFormatter()
            if let date = dateFormatter.date(from: dateStr) {
                return date
            }
            dateFormatter.formatOptions.insert(.withFractionalSeconds)
            if let date = dateFormatter.date(from: dateStr) {
                return date
            } else {
                throw DecodingError.dataCorruptedError(in: container, debugDescription: "Cannot decode date string \(dateStr)")
            }
        }
    }
}
