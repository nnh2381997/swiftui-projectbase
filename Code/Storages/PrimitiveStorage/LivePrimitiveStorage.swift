//
//  LivePrimitiveStorage.swift
//  Common
//
//  Created by Dwight Everhart on 10/24/21.
//  Copyright © 2021 TaylorMade Golf. All rights reserved.
//

import KeychainAccess

public extension PrimitiveKey where ValueType == String {
    static let keychainAccessCodeKey = PrimitiveKey("keychainAccessCode")
}

public final class LivePrimitiveStorage: PrimitiveStorage {
    public var secureStorage: PrimitiveSubscriptable
    public var insecureStorage: PrimitiveSubscriptable

    public init(defaults: UserDefaults) {
        // We use a random UUID to control access to the keychain, so that all values
        // in the keychain will become inaccessable if the user uninstalls the app.
        let keychainAccessCode = defaults[.keychainAccessCodeKey] ?? UUID().uuidString
        defaults[.keychainAccessCodeKey] = keychainAccessCode
        secureStorage = Keychain(service: keychainAccessCode)
        insecureStorage = defaults
    }
}

extension UserDefaults: PrimitiveSubscriptable {
    public subscript(key: PrimitiveKey<String>) -> String? {
        get {
            string(forKey: key.name)
        }
        set {
            set(newValue, forKey: key.name)
        }
    }

    public subscript(key: PrimitiveKey<Bool>) -> Bool? {
        get {
            bool(forKey: key.name)
        }
        set {
            set(newValue, forKey: key.name)
        }
    }

    public subscript(key: PrimitiveKey<Int>) -> Int? {
        get {
            integer(forKey: key.name)
        }
        set {
            set(newValue, forKey: key.name)
        }
    }

    public subscript(key: PrimitiveKey<Int64>) -> Int64? {
        get {
            guard let stringValue = string(forKey: key.name) else { return nil }
            return Int64(stringValue)
        }
        set {
            if let newValue = newValue {
                set("\(newValue)", forKey: key.name)
            } else {
                set(nil, forKey: key.name)
            }
        }
    }

    public subscript(key: PrimitiveKey<Float>) -> Float? {
        get {
            float(forKey: key.name)
        }
        set {
            set(newValue, forKey: key.name)
        }
    }

    public subscript(key: PrimitiveKey<Double>) -> Double? {
        get {
            double(forKey: key.name)
        }
        set {
            set(newValue, forKey: key.name)
        }
    }

    public subscript(key: PrimitiveKey<Date>) -> Date? {
        get {
            object(forKey: key.name) as! Date?
        }

        set {
            set(newValue, forKey: key.name)
        }
    }

    public subscript(key: String) -> Any? {
        get {
            object(forKey: key)
        }
        set {
            set(newValue, forKey: key)
        }
    }
}

extension Keychain: PrimitiveSubscriptable {
    public subscript(key: PrimitiveKey<String>) -> String? {
        get {
            self[string: key.name]
        }
        set {
            self[string: key.name] = newValue
        }
    }

    public subscript(key: PrimitiveKey<Bool>) -> Bool? {
        get {
            guard let stringValue = self[string: key.name] else { return nil }
            return Bool(stringValue)
        }
        set {
            if let newValue = newValue {
                self[string: key.name] = newValue ? "true" : "false"
            } else {
                self[string: key.name] = nil
            }
        }
    }

    public subscript(key: PrimitiveKey<Int>) -> Int? {
        get {
            guard let stringValue = self[string: key.name] else { return nil }
            return Int(stringValue)
        }
        set {
            if let newValue = newValue {
                self[string: key.name] = "\(newValue)"
            } else {
                self[string: key.name] = nil
            }
        }
    }

    public subscript(key: PrimitiveKey<Int64>) -> Int64? {
        get {
            guard let stringValue = self[string: key.name] else { return nil }
            return Int64(stringValue)
        }
        set {
            if let newValue = newValue {
                self[string: key.name] = "\(newValue)"
            } else {
                self[string: key.name] = nil
            }
        }
    }

    public subscript(key: PrimitiveKey<Float>) -> Float? {
        get {
            guard let stringValue = self[string: key.name] else { return nil }
            return Float(stringValue)
        }
        set {
            if let newValue = newValue {
                self[string: key.name] = "\(newValue)"
            } else {
                self[string: key.name] = nil
            }
        }
    }

    public subscript(key: PrimitiveKey<Double>) -> Double? {
        get {
            guard let stringValue = self[string: key.name] else { return nil }
            return Double(stringValue)
        }
        set {
            if let newValue = newValue {
                self[string: key.name] = "\(newValue)"
            } else {
                self[string: key.name] = nil
            }
        }
    }

    public subscript(key: PrimitiveKey<Date>) -> Date? {
        get {
            guard let stringValue = self[string: key.name] else { return nil }
            return ISO8601DateFormatter().date(from: stringValue)
        }

        set {
            if let newValue = newValue {
                self[string: key.name] = ISO8601DateFormatter().string(from: newValue)
            } else {
                self[string: key.name] = nil
            }
        }
    }

    public subscript(key: String) -> Any? {
        get {
            assertionFailure("subscript(key: String) is not implemented for Keychain")
        }
        set {
            assertionFailure("subscript(key: String) is not implemented for Keychain")
        }
    }

}
