//
//  MockPrimitiveStorage.swift
//  CommonTests
//
//  Created by Dwight Everhart on 11/1/21.
//  Copyright © 2021 TaylorMade Golf. All rights reserved.
//

import Foundation

public class MockSubscriptable: PrimitiveSubscriptable, CustomDebugStringConvertible {
    public var debugDescription: String {
        return String(describing: values)
    }

    private var values = [String : Any]()

    public init() {
    }

    public subscript(key: PrimitiveKey<Double>) -> Double? {
        get {
            return values[key.name] as? Double
        }
        set {
            values[key.name] = newValue
        }
    }

    public subscript(key: PrimitiveKey<Float>) -> Float? {
        get {
            return values[key.name] as? Float
        }
        set {
            values[key.name] = newValue
        }
    }

    public subscript(key: PrimitiveKey<Int>) -> Int? {
        get {
            return values[key.name] as? Int
        }
        set {
            values[key.name] = newValue
        }
    }

    public subscript(key: PrimitiveKey<Int64>) -> Int64? {
        get {
            return values[key.name] as? Int64
        }
        set {
            values[key.name] = newValue
        }
    }

    public subscript(key: PrimitiveKey<Bool>) -> Bool? {
        get {
            return values[key.name] as? Bool
        }
        set {
            values[key.name] = newValue
        }
    }

    public subscript(key: PrimitiveKey<String>) -> String? {
        get {
            return values[key.name] as? String
        }
        set {
            values[key.name] = newValue
        }
    }

    public subscript(key: PrimitiveKey<Date>) -> Date? {
        get {
            return values[key.name] as? Date
        }
        set {
            values[key.name] = newValue
        }
    }

    public subscript(key: String) -> Any? {
        get {
            return values[key]
        }
        set {
            values[key] = newValue
        }
    }
}

public final class MockPrimitiveStorage: PrimitiveStorage {
    public var secureStorage: PrimitiveSubscriptable = MockSubscriptable()
    public var insecureStorage: PrimitiveSubscriptable = MockSubscriptable()

    public init() {
    }
}
