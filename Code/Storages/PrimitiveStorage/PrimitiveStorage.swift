//
//  PrimitiveStorage.swift
//  Common
//
//  Created by Dwight Everhart on 10/24/21.
//  Copyright © 2021 TaylorMade Golf. All rights reserved.
//

public protocol PrimitiveStorage: AnyObject {
    var secureStorage: PrimitiveSubscriptable { get set }
    var insecureStorage: PrimitiveSubscriptable { get set }
}
