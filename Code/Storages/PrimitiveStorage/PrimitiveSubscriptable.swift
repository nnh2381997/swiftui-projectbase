//
//  PrimitiveSubscriptable.swift
//  Common
//
//  Created by Dwight Everhart on 10/24/21.
//  Copyright © 2021 TaylorMade Golf. All rights reserved.
//

import Foundation

public struct PrimitiveKey<ValueType>: ExpressibleByStringLiteral {
    public let name: String

    public init(_ name: String) {
        self.name = name
    }

    public init(stringLiteral value: String) {
        self.name = value
    }
}

public protocol PrimitiveSubscriptable {
    subscript(_ key: PrimitiveKey<String>) -> String? { get set }
    subscript(_ key: PrimitiveKey<Bool>) -> Bool? { get set }
    subscript(_ key: PrimitiveKey<Int>) -> Int? { get set }
    subscript(_ key: PrimitiveKey<Int64>) -> Int64? { get set }
    subscript(_ key: PrimitiveKey<Float>) -> Float? { get set }
    subscript(_ key: PrimitiveKey<Double>) -> Double? { get set }
    subscript(_ key: PrimitiveKey<Date>) -> Date? { get set }
    subscript(_ key: String) -> Any? { get set }
}
