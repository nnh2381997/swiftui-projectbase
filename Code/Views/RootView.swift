//
//  RootView.swift
//  SwiftUI-ProjectBase
//
//  Created by HienNN on 17/08/2023.
//

import SwiftUI

struct RootView: View {
    @AppStorage(AppLanguages.APPLANGUAGE) var language: String = AppLanguages.en.rawValue
    @State var showDropDown: Bool = false
    @State var valueField: String = ""
    @State var isStartAnimation: Bool = false
    
    
    var body: some View {
        VStack(spacing: 20) {
            Text(Localized.General.hello)
            
            DropDownView(
                title: Localized.General.languages,
                value: AppLanguages(rawValue: language),
                data: AppLanguages.allCases,
                noDataText: "",
                showDropDown: $showDropDown,
                setValue: { value in
                    AppLanguages.saveAppLanguage(value)
                }
            )
            
            TextFieldView(title: "Name", valueField: $valueField)
            
            MusicDiscImage(isStartAnimation: $isStartAnimation)
            
            Button(action: {
                isStartAnimation.toggle()
            }) {
                Text("action")
            }
            
            Spacer()
        }
        .onChange(of: language, perform: { _ in })
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding()
        .background(.black.opacity(0.3))
    }
}

struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
