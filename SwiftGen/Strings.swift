// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
public enum Localized {

  public enum General {
    /// Hello word
    public static var hello: String { Localized.tr("Localizable", "general.hello") }
    /// LANGUAGES
    public static var languages: String { Localized.tr("Localizable", "general.languages") }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details
extension Localized {
    private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
        let localeIdentifier = AppLanguages.getAppLanguage.rawValue
        guard let path = Bundle.main.path(forResource: localeIdentifier, ofType: "lproj"),
              let bundle = Bundle(path: path) else {
                    let format = Bundle.main.localizedString(forKey: key, value: nil, table: table)
                    return String(format: format, locale: Locale.current, arguments: args)
                }
        let format = bundle.localizedString(forKey: key, value: nil, table: table)
        return String(format: format, locale: Locale.current, arguments: args)
    }
}
