//
//  iOSApp.swift
//  iOS
//
//  Created by HienNN on 17/08/2023.
//

import SwiftUI
import IQKeyboardManagerSwift

@main
struct iOSApp: App {
    var body: some Scene {
        WindowGroup {
            RootView()
                .onAppear {
                    IQKeyboardManager.shared.enable = true
                    IQKeyboardManager.shared.keyboardDistanceFromTextField = 10
                }
        }
    }
}
