//
//  macOSApp.swift
//  macOS
//
//  Created by HienNN on 17/08/2023.
//

import SwiftUI

@main
struct macOSApp: App {
    private var iPhone11: (width: CGFloat, height: CGFloat) {
        return (width: 414, height: 896)
    }
    
    var body: some Scene {
        WindowGroup {
            RootView()
                .frame(
                    minWidth: iPhone11.width, maxWidth: iPhone11.width,
                    minHeight: iPhone11.height, maxHeight: iPhone11.height,
                    alignment: .center
                )
        }
        .windowStyle(HiddenTitleBarWindowStyle())
    }
}
